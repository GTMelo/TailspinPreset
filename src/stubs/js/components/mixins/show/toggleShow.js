export const toggleShow = {

    data(){
        return {
            isShowing:true
        }
    },

    methods: {
        toggleShow: function (target = this) {
            target.isShowing = !target.isShowing;
        },
    }
};