let mix = require('laravel-mix');

const glob = require('glob-all');

require('laravel-mix-tailwind');
require('laravel-mix-purgecss');

mix.js('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .tailwind()
    .purgeCss()
    .browserSync('http://your-site-here.dev/')
    .copyDirectory('resources/assets/img', 'public/img')
    .options({
        postCss: [
            require ('cssnano')({
                preset: 'default',
            })
        ]
    });
