<?php

namespace GTMelo\TailspinPreset;

use Illuminate\Foundation\Console\Presets\Preset as LaravelPreset;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\File;

class Preset extends LaravelPreset
{
    private static $isPackageDev = true;

    public static function install()
    {

        self::setupPackages();
        self::setupDirectories();
        self::setupMix();
        self::setupTailwind();
        self::setupSass();
        self::setupJs();

    }

    public static function devPackages()
    {
        return [
            "axios" => "^0.18",
            "browser-sync" => "^2.24.7",
            "browser-sync-webpack-plugin" => "^2.0.1",
            "cssnano" => "^4.1.0",
            "glob-all" => "^3.1",
            "laravel-mix" => "^2.0",
            "laravel-mix-purgecss" => "^2.2.0",
            "laravel-mix-tailwind" => "^0.1.0",
            "postcss-import" => "^11.1.0",
            "postcss-nesting" => "^5.0.0",
            "tailwindcss" => ">=0.5.3",
            "tailwindcss-alpha" => "^0.1.2",
            "vue" => "^2.5.17"
        ];
    }

    public static function prodPackages()
    {
        return [
            'animejs' => '^2.2.0',
            'vue-animejs' => '^1.0.1',
            'concat-map' => '0.0.1',
            'is-fullwidth-code-point' => '^2.0.0',
            "@fortawesome/fontawesome-free" => "^5.2.0",
        ];
    }

    public static function updatePackageArray($packages)
    {

        $arrayToUse = (self::$isPackageDev ? self::devPackages() : self::prodPackages());
        return $arrayToUse + Arr::except($packages, self::toRemove());

    }

    public static function toRemove()
    {
        return [
            'jquery',
            'bootstrap',
            'bulma',
        ];
    }

    public static function setupPackages()
    {
        self::updatePackages();
        self::$isPackageDev = false;
        self::updatePackages(false);
    }

    public static function setupDirectories()
    {
        File::cleanDirectory(resource_path('assets/sass'));
        File::cleanDirectory(resource_path('assets/js'));
        File::makeDirectory(resource_path('assets/img'));
    }

    public static function setupMix()
    {
        copy(__DIR__ . '/stubs/webpack.mix.js', base_path('webpack.mix.js'));
    }

    public static function setupTailwind()
    {
        copy(__DIR__ . '/stubs/tailwind.js', base_path('tailwind.js'));
    }

    public static function setupSass()
    {
        self::recursive_copy(__DIR__ . '/stubs/sass', resource_path('assets/sass'));
    }

    public static function setupJs()
    {
        self::recursive_copy(__DIR__ . '/stubs/js', resource_path('assets/js'));
    }

    public static function recursive_copy($source, $destination)
    {
        $dir = opendir($source);
        @mkdir($destination);
        while (false !== ($file = readdir($dir))) {
            if (($file != '.') && ($file != '..')) {
                if (is_dir($source . '/' . $file)) {
                    self::recursive_copy($source . '/' . $file, $destination . '/' . $file);
                } else {
                    copy($source . '/' . $file, $destination . '/' . $file);
                }
            }
        }
        closedir($dir);
    }

}